import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:location_transmitter/location_transmitter.dart';
import 'package:location_transmitter/src/location_transmitter_api.dart';

class LocationTransmitter {
  Timer _timer;
  final LocationStore store;
  final LocationTransmitterApi transmitterApi;
  final Duration period;
  final LocationManager locationManager;
  StreamSubscription<LocationRecord> _locationStreamSubscription;

  LocationTransmitter(
      {this.period = const Duration(seconds: 30),
      @required this.locationManager,
      @required this.store,
      @required this.transmitterApi}) {
    _init();
  }

  _init() async {
    await store.init();
    start();
  }

  _flushTimer(Timer timer) async {
    final records = await store?.getUnflushedRecords();
    try {
      print("${records.length} records to flush");
      await transmitterApi.push(records);
      print("Records flushed successfully");
      store.markRecordsAsFlushed(records);
    } catch (e) {
      print("Error flushing: $e");
    }
  }

  void start() {
    _locationStreamSubscription =
        locationManager.onLocationChanged().listen((event) {
      store?.add(event);
    });
    _timer = Timer.periodic(period, _flushTimer);
  }

  void stop() {
    _locationStreamSubscription?.cancel();
    _locationStreamSubscription = null;
    _timer.cancel();
    _timer = null;
  }
}
