import 'package:location_transmitter/location_transmitter.dart';

abstract class LocationTransmitterApi {
  push(List<LocationRecord> records);
}

class LocationTransmitterApiException implements Exception {
  String cause;

  LocationTransmitterApiException(this.cause);

  @override
  String toString() {
    return 'LocationTransmitterApiException{cause: $cause}';
  }
}
