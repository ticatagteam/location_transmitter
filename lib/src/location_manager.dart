import 'dart:async';

import 'package:device_information/device_information.dart';
import 'package:location/location.dart';
import 'package:location_transmitter/location_transmitter.dart';

class LocationManager {
  static final LocationManager _instance = LocationManager._internal();
  final Location _location = Location();
  String _deviceId;

  LocationManager._internal() {
    _init();
  }

  _init() async {
    _deviceId = await DeviceInformation.deviceIMEINumber;
    _location.enableBackgroundMode(enable: true);
    _location.changeSettings(
        accuracy: LocationAccuracy.high, interval: 10000, distanceFilter: 10);

    bool serviceEnabled = await _location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await _location.requestService();
      if (!serviceEnabled) {
        return;
      }
    }

    PermissionStatus permissionGranted = await _location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await _location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
  }

  Stream<LocationRecord> onLocationChanged() {
    return _location.onLocationChanged.map((event) => LocationRecord(
        longitude: event.longitude,
        latitude: event.latitude,
        altitude: event.altitude,
        deviceId: _deviceId ?? "unknown",
        timestamp: DateTime.fromMillisecondsSinceEpoch(event.time.toInt()),
        speed: event.speed * 3.6,
        accuracy: event.verticalAccuracy,
        meta: {},
        satelliteNumber: event.satelliteNumber ?? 0));
  }

  static LocationManager get instance => _instance;
}
