import 'dart:async';
import 'dart:io';

import 'package:location_transmitter/location_transmitter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

typedef OnBeforeSave = Future<LocationRecord> Function(LocationRecord record);

class LocationStore {
  static const kLocationStore = 'locations';
  OnBeforeSave onBeforeSave;
  static final LocationStore _singleton = LocationStore._internal();
  static Database _db;
  static final StreamController<List<LocationRecord>> _recordsStreamController =
      StreamController.broadcast();
  Stream<List<LocationRecord>> get recordsStream =>
      _recordsStreamController.stream;

  factory LocationStore() {
    return _singleton;
  }

  LocationStore._internal();

  Future<void> init() async {
    if (_db == null) {
      var appDirectory = await getApplicationDocumentsDirectory();
      DatabaseFactory dbFactory = databaseFactoryIo;
      _db = await dbFactory.openDatabase('${appDirectory.path}/locations1.db');
    }
  }

  static LocationStore get instance {
    return _singleton;
  }

  void clear() async {
    await _db?.close();
    var appDirectory = await getApplicationDocumentsDirectory();
    var filePath = '${appDirectory.path}/beacons_location.db';
    File f = File.fromUri(Uri.file(filePath));
    await f.delete();
  }

  clearStore() async {
    try {
      StoreRef storeRef = stringMapStoreFactory.store(kLocationStore);
      final records = await storeRef.find(_db, finder: Finder());
      final recordsIds = records.map((record) => record.key).toList();

      await _db.transaction((txn) async {
        List<Future> futures = [];
        recordsIds.forEach((key) => futures.add(storeRef.delete(key)));
        await Future.wait(futures);
      });
    } catch (e) {
      throw LocationStoreException('[clearStore] $e');
    }
  }

  Future<void> remove(LocationRecord beaconRecord) async {
    try {
      StoreRef storeRef = stringMapStoreFactory.store(kLocationStore);
      final record = storeRef.record(int.parse(beaconRecord.id));
      await record.delete(_db);
    } catch (e) {
      throw LocationStoreException('[remove] $e');
    }
  }

  Future<void> add(LocationRecord beaconRecord) async {
    StoreRef storeRef = stringMapStoreFactory.store(kLocationStore);
    if (onBeforeSave != null) {
      final enrichedRecord = await onBeforeSave(beaconRecord);
      storeRef.add(_db, enrichedRecord.toJson());
    } else {
      storeRef.add(_db, beaconRecord.toJson());
    }
    _recordsStreamController.add([beaconRecord]);
  }

  Future<List<LocationRecord>> getAll({int limit = 150, bool isFlushed}) async {
    try {
      StoreRef storeRef = stringMapStoreFactory.store(kLocationStore);
      Finder finder = Finder();

      var filters = [];
      if (isFlushed != null) {
        filters.add(Filter.equals("isFlushed", isFlushed));
      }

      if (filters.isNotEmpty) {
        finder.filter = Filter.and(filters);
      }
      finder.sortOrder = SortOrder('timestamp', false, true);
      finder.limit = limit;
      final records = await storeRef.find(_db, finder: finder);
      List<LocationRecord> beaconLocationRecords = records
          .map((record) =>
              LocationRecord.fromJson(record.key.toString(), record.value))
          .toList();
      return beaconLocationRecords;
    } catch (e) {
      throw LocationStoreException('[getAll] $e');
    }
  }

  Future<LocationRecord> get(String macAddress) async {
    try {
      StoreRef storeRef = stringMapStoreFactory.store(kLocationStore);
      Finder finder = Finder();
      finder.filter = Filter.equals('macAddress', macAddress);
      finder.sortOrder = SortOrder('timestamp', false, true);
      finder.limit = 1;
      final record = await storeRef.findFirst(_db, finder: finder);
      if (record != null) {
        LocationRecord beaconLocationRecord =
            LocationRecord.fromJson(record.key, record.value);
        return beaconLocationRecord;
      } else {
        return null;
      }
    } catch (e) {
      throw LocationStoreException('[get] $e');
    }
  }

  Future<void> delete() async {
    try {
      StoreRef storeRef = stringMapStoreFactory.store(kLocationStore);

      Finder finder = Finder();
      finder.filter = Filter.equals('isFlushed', true);
      final res = await storeRef.find(_db, finder: finder);
      final recordsIds = res.map((record) => record.key).toList();

      await _db.transaction((txn) async {
        List<Future> futures = [];
        recordsIds.forEach((key) => futures.add(storeRef.delete(key)));
        await Future.wait(futures);
      });
    } catch (e) {
      throw LocationStoreException('[delete] $e');
    }
  }

  Future<List<LocationRecord>> getUnflushedRecords() async {
    try {
      StoreRef storeRef = stringMapStoreFactory.store(kLocationStore);
      Finder finder = Finder();
      finder.filter = Filter.equals('isFlushed', false);
      var res = await storeRef.find(_db, finder: finder);
      var records = res
          .map((record) =>
              LocationRecord.fromJson(record.key.toString(), record.value))
          .toList();
      print('${records.length} records to flush on server.');
      return records;
    } catch (e) {
      throw LocationStoreException('[getUnflushedRecords] $e');
    }
  }

  void markRecordsAsFlushed(List<LocationRecord> records) async {
    List<String> recordsIds =
        records.map((LocationRecord record) => record.id).toList();
    StoreRef storeRef = stringMapStoreFactory.store(kLocationStore);

    try {
      for (final key in recordsIds) {
        final record = storeRef.record(key);
        await record.store.update(_db, {'isFlushed': true});
      }
    } catch (e) {
      throw LocationStoreException('[markRecordsAsFlushed] $e');
    }
  }

  void dispose() {
    _recordsStreamController?.close();
  }
}

class LocationStoreException implements Exception {
  String cause;
  LocationStoreException(this.cause);

  @override
  String toString() {
    return 'BeaconLocationStoreException{cause: $cause}';
  }
}
