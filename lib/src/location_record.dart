class LocationRecord {
  final String deviceId;
  final double accuracy;
  final double longitude;
  final double latitude;
  final double altitude;
  final int satelliteNumber;
  final double speed;
  final DateTime timestamp;
  final bool isFlushed;
  final Map<String, dynamic> meta;
  String id;

  LocationRecord(
      {this.deviceId,
      this.longitude,
      this.latitude,
      this.timestamp,
      this.altitude,
      this.satelliteNumber,
      this.speed,
      this.accuracy,
      this.meta})
      : isFlushed = false;

  LocationRecord.fromJson(this.id, Map<dynamic, dynamic> map)
      : timestamp = DateTime.fromMillisecondsSinceEpoch(map['timestamp']),
        deviceId = map['deviceId'],
        isFlushed = map['isFlushed'],
        speed = map['speed'],
        satelliteNumber = map['satelliteNumber'],
        latitude = map['location']['latitude'],
        longitude = map['location']['longitude'],
        altitude = map['location']['altitude'],
        accuracy = map['location']['accuracy'],
        meta = map['meta'];

  Map<String, dynamic> toJson() {
    return {
      'isFlushed': isFlushed,
      'timestamp': timestamp?.millisecondsSinceEpoch,
      'deviceId': deviceId,
      'speed': speed,
      'satelliteNumber': satelliteNumber,
      'location': {
        'latitude': latitude,
        'longitude': longitude,
        'altitude': altitude,
        'accuracy': accuracy
      },
      'meta': meta
    };
  }

  @override
  String toString() {
    return 'LocationRecord{deviceId: $deviceId, accuracy: $accuracy, longitude: $longitude, latitude: $latitude, altitude: $altitude, satelliteNumber: $satelliteNumber, speed: $speed, timestamp: $timestamp, isFlushed: $isFlushed, meta: $meta, id: $id}';
  }
}
